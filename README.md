# Documents generator

## Developer setup

### Initial setup

You will need `docker` and `docker-compose` installed on your computer.

`poetry` is not a strict requirement to develop, but it will become very handy
for things like adding dependencies, or pre-commit hooks.

```sh
$ poetry install
$ cd docker/
$ ./gen_secrets.sh
$ DOCKER_BUILDKIT=1 COMPOSE_DOCKER_CLI_BUILD=1 docker-compose -p docgen up --build
```

Now, let's get you admin rights.

```sh
# Open a new terminal, leaving docker-compose running in this one to get logs
$ cd docker/
$ ./manage.sh createsuperuser
```

Go to http://localhost:8000/admin and log in with your newly created user.

### Regular usage

```sh
$ cd docker/
$ DOCKER_BUILDKIT=1 COMPOSE_DOCKER_CLI_BUILD=1 docker-compose -p docgen up --build
```

### Formatting

We use `python/black` as code formatter. It can be enabled through git
pre-commit with the following command:

```sh
$ poetry run pre-commit install
```

### Linting

We also use `pylint` (via `prospector`) to check the coding style. It is
configured with GitLab CI and produces warnings in GitLab. To check if the
project matches `pylint`'s recommendations you can use the following command:

```sh
$ poetry run prospector --profile base
```

### Services

#### Adminer
    URL: http://localhost:8010/
    System: 'PostgreSQL'
    User: 'documents_generator_dev'
    Password in: docker/secrets/postgres-passwd
    Database: 'documents_generator_dev'

    Have a look in `docker/docker-compose.yml` for other databases and
    credentials location.

#### Website
    URL: http://localhost:8000/
