from django.urls import include, path
from django_utils.urls import default, drf

urlpatterns = (
    default.urlpatterns(with_pprof=True)
    + drf.urlpatterns(apps_with_api=["generator"], with_auth=True)
    + [
        path("", include("generator.urls")),
        path("debug/pprof/", include("django_pypprof.urls")),
    ]
)
