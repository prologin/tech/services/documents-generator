from django.urls import include, path
from rest_framework.routers import DefaultRouter

from .viewsets import DefaultValueViewSet, TemplateViewSet

router = DefaultRouter()
router.root_view_name = "generator-api-root"
router.register("templates", TemplateViewSet)
router.register("default-values", DefaultValueViewSet)


urlpatterns = [
    path("", include(router.urls)),
]
