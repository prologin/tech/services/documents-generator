from rest_framework import serializers

from .models import DefaultValue, Template


class DefaultValueSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = DefaultValue
        # We need to overwrite fields to include id
        fields = (
            "url",
            "id",
            "template",
            "values",
        )


class TemplateSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Template
        # We need to overwrite fields to include id
        fields = (
            "url",
            "id",
            "name",
            "slug",
            "template",
            "jsonschema",
            "public",
        )
