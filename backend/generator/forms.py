from django import forms

from .models import Template


class TemplateChoiceForm(forms.Form):
    template = forms.ModelChoiceField(
        queryset=Template.objects.all(),
        empty_label="(Choose a template)",
        to_field_name="slug",
    )
