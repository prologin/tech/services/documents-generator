from django.contrib import admin
from django.contrib.auth.decorators import login_required

from .models import DefaultValue, Template

admin.site.login = login_required(admin.site.login)


@admin.register(DefaultValue)
class DefaultValueAdmin(admin.ModelAdmin):
    list_display = ("template",)
    list_display_links = list_display
    list_filter = ("template",)
    search_fields = list_display


class DefaultValueInline(admin.StackedInline):
    model = DefaultValue


@admin.register(Template)
class TemplateAdmin(admin.ModelAdmin):
    list_display = ("name", "public")
    list_display_links = list_display
    list_filter = ("public",)
    search_fields = list_display
    prepopulated_fields = {
        "slug": ("name",),
    }
    inlines = (DefaultValueInline,)
