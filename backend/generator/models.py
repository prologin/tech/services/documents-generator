import os
import re
import subprocess
import tempfile
import uuid

import jinja2
import jsonschema
from django.core.exceptions import ValidationError
from django.db import models

LATEX_ESCAPE_CHARS = {
    "&": r"\&",
    "%": r"\%",
    "$": r"\$",
    "#": r"\#",
    "_": r"\_",
    "{": r"\{",
    "}": r"\}",
    "^": r"\^{}",
    "~": r"\textasciitilde{}",
    "\n": r"\\",
    "\\": r"\textbackslash{}",
    "<": r"\textless",
    ">": r"\textgreater",
}

# Combine all replacements into a single pattern
LATEX_ESCAPE_REGEX = re.compile(
    "|".join(
        re.escape(key)
        for key in sorted(LATEX_ESCAPE_CHARS.keys(), key=len, reverse=True)
    )
)


def latexescape(value: str) -> str:
    if not isinstance(value, str):
        return value

    return LATEX_ESCAPE_REGEX.sub(
        lambda match: LATEX_ESCAPE_CHARS[match.group()],
        value,
    )


def preventempty(value: str) -> str:
    if value == "":
        return "~"  # unbreakable space
    return value


def dict_merge(dct, merge_dct):
    for k, _ in merge_dct.items():
        if isinstance(dct.get(k), dict) and isinstance(merge_dct[k], dict):
            dict_merge(dct[k], merge_dct[k])
        else:
            dct[k] = merge_dct[k]


def validate_jsonschema(value):
    validator = jsonschema.validators.validator_for(value)
    try:
        validator.check_schema(value)
    except jsonschema.exceptions.SchemaError as e:
        raise ValidationError(
            str(e),
            params={"value": value},
        ) from e


def create_pdf(jobname, src):
    src = src.encode("utf-8")
    jobname = f"{jobname}-{uuid.uuid4()}"
    with tempfile.TemporaryDirectory() as td:
        args = [
            "pdflatex",
            f"-output-directory={td}",
            f"-jobname={jobname}",
            "-output-format=pdf",
        ]
        fp = subprocess.run(
            args,
            input=src,
            timeout=15,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            check=False,
        )
        try:
            with open(os.path.join(td, f"{jobname}.pdf"), "rb") as f:
                pdf = f.read()
        except FileNotFoundError as e:
            raise RuntimeError(fp.stdout + fp.stderr) from e
    return pdf


class Template(models.Model):
    name = models.CharField(max_length=255)
    slug = models.SlugField(max_length=255, unique=True)

    template = models.TextField()

    jsonschema = models.JSONField(
        default=dict,
        validators=(validate_jsonschema,),
        help_text="Use https://json-editor.github.io/json-editor/ to generate your schema.",  # noqa: E501
    )

    public = models.BooleanField(default=False)

    def get_default_values(self):
        values = {}
        for default_values in DefaultValue.objects.filter(template=None):
            dict_merge(values, default_values.values)
        for default_values in DefaultValue.objects.filter(template=self):
            dict_merge(values, default_values.values)
        return values

    def render(self, values):
        ctx = self.get_default_values()
        dict_merge(ctx, values)

        ctx = dict(
            map(
                lambda kv: (kv[0], preventempty(latexescape(kv[1]))),
                ctx.items(),
            )
        )

        env = jinja2.Environment()

        rendered = env.from_string(self.template).render(**ctx)

        return create_pdf(self.slug, rendered)

    def __str__(self):
        return self.name


class DefaultValue(models.Model):
    template = models.OneToOneField(
        Template,
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        db_index=True,
    )

    values = models.JSONField(
        default=dict,
        help_text="Values will be merged with this order of priority (descending): user provided values, template default values, default values with no template attached.",  # noqa: E501
    )

    def __str__(self):
        if self.template:
            return f"{self.template}"
        return "Global default"
