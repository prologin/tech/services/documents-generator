from django.urls import path

from .views import TemplateChoiceView, TemplateView

urlpatterns = [
    path("", TemplateChoiceView.as_view(), name="home"),
    path("template/<str:slug>/", TemplateView.as_view(), name="template"),
]
