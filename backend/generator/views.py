from django.http import HttpResponse, HttpResponseServerError
from django.urls import reverse
from django.views.generic.detail import SingleObjectMixin
from django.views.generic.edit import FormView
from django_jsonforms.forms import JSONSchemaForm

from .forms import TemplateChoiceForm
from .models import Template


class TemplateChoiceView(FormView):
    template_name = "template_choice.html"
    form_class = TemplateChoiceForm

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        if not self.request.user.is_authenticated:
            form.fields["template"].queryset = form.fields[
                "template"
            ].queryset.filter(public=True)
        return form

    def get_success_url(self):
        form = self.get_form()
        form.is_valid()  # noop as it was already called before, and thus always returns True
        return reverse(
            "template", kwargs={"slug": form.cleaned_data["template"].slug}
        )


class TemplateView(SingleObjectMixin, FormView):
    template_name = "template.html"
    model = Template

    def get_queryset(self):
        queryset = super().get_queryset()
        if not self.request.user.is_authenticated:
            queryset = queryset.filter(public=True)
        return queryset

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().post(request, *args, **kwargs)

    def put(self, *args, **kwargs):
        self.object = self.get_object()
        return super().put(*args, **kwargs)

    def get_form(self, form_class=None):
        form = JSONSchemaForm(
            schema=self.get_object().jsonschema,
            options={
                "theme": "html",
                "disable_edit_json": True,
                "disable_properties": True,
                "disable_collapse": True,
                "required_by_default": True,
                "object_layout": "grid",
            },
            ajax=False,
            **self.get_form_kwargs(),
        )
        form.fields["json"].label = False

        return form

    def form_valid(self, form):
        form.is_valid()  # noop as it was already called before, and thus always returns True
        values = form.cleaned_data["json"]
        try:
            rendered = self.object.render(values)
        except RuntimeError as e:
            return HttpResponseServerError(e)
        return HttpResponse(
            rendered,
            headers={
                "Content-Type": "application/pdf",
                "Content-disposition": f'attachment; filename="{self.object.slug}.pdf"',
            },
        )
