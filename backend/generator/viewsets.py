from django.http import HttpResponse, HttpResponseServerError
from rest_framework.decorators import action, parser_classes
from rest_framework.parsers import JSONParser
from rest_framework.permissions import (
    AllowAny,
    DjangoModelPermissionsOrAnonReadOnly,
)
from rest_framework.viewsets import ModelViewSet

from .models import DefaultValue, Template
from .serializers import DefaultValueSerializer, TemplateSerializer


class TemplateViewSet(ModelViewSet):
    queryset = Template.objects.all()
    serializer_class = TemplateSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]

    def get_queryset(self):
        queryset = super().get_queryset()
        if not self.request.user.is_authenticated:
            queryset = queryset.filter(public=True)
        return queryset

    @action(detail=True, methods=["post"], permission_classes=[AllowAny])
    @parser_classes([JSONParser])
    def render(self, request, *args, **kwargs):
        template = self.get_object()
        try:
            rendered = template.render(request.data)
        except RuntimeError as e:
            return HttpResponseServerError(e)
        return HttpResponse(
            rendered,
            headers={
                "Content-Type": "application/pdf",
                "Content-disposition": f'attachment; filename="{template.slug}.pdf"',
            },
        )


class DefaultValueViewSet(ModelViewSet):
    queryset = DefaultValue.objects.all()
    serializer_class = DefaultValueSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]

    def get_queryset(self):
        queryset = super().get_queryset()
        if not self.request.user.is_authenticated:
            queryset = queryset.filter(template__public=True)
        return queryset
